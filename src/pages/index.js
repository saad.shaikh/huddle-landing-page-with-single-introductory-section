import React, { Fragment, useEffect } from "react"
import { Helmet } from "react-helmet"
import Logo from "../images/logo.svg"
import IllustrationMockups from "../images/illustration-mockups.svg"
import Facebook from "../images/facebook.svg"
import FacebookHover from "../images/facebook-hover.svg"
import Twitter from "../images/twitter.svg"
import TwitterHover from "../images/twitter-hover.svg"
import Instagram from "../images/instagram.svg"
import InstagramHover from "../images/instagram-hover.svg"
import "../index.css"

const IndexPage = () => {

  let socialsDimenstions = "55.5"

  useEffect(() => {
    document.getElementById("facebook").addEventListener("mouseover", (e) => {
      e.target.src = FacebookHover;
    })
    document.getElementById("facebook").addEventListener("mouseleave", (e) => {
      e.target.src = Facebook;
    })
    document.getElementById("twitter").addEventListener("mouseover", (e) => {
      e.target.src = TwitterHover;
    })
    document.getElementById("twitter").addEventListener("mouseleave", (e) => {
      e.target.src = Twitter;
    })
    document.getElementById("instagram").addEventListener("mouseover", (e) => {
      e.target.src = InstagramHover;
    })
    document.getElementById("instagram").addEventListener("mouseleave", (e) => {
      e.target.src = Instagram;
    })
  })

  return (
    <Fragment>

      <Helmet title="Huddle landing page with single introductory section" />
      <img className="logo" src={Logo} alt="Logo"></img>
      <main>
        <img className="illustrationMockups" src={IllustrationMockups} alt="Illustration"></img>
        <div className="text">
          <h1>Build The Community Your Fans Will Love</h1>
          <p className="p">
            Huddle re-imagines the way we build communities. You have a voice, but so does your audience. Create connections with your users as you engage in genuine discussion.
          </p>
          <button>Register</button>
        </div>
      </main>

      <footer>
        <div className="socials">
          <img id="facebook" src={Facebook} height={socialsDimenstions} width={socialsDimenstions} alt="Facebook"></img>
          <img id="twitter" src={Twitter} height={socialsDimenstions} width={socialsDimenstions} alt="Twitter"></img>
          <img id="instagram" src={Instagram} height={socialsDimenstions} width={socialsDimenstions} alt="Instagram"></img>
        </div>
        <p class="attribution">
          Challenge by <a href="https://www.frontendmentor.io/solutions/huddle-landing-page-with-a-single-introductory-section-gatsby-gZvaoZhEFE" target="_blank" rel="noreferrer">Frontend Mentor</a>.
          Coded by <a href="https://saad-shaikh-portfolio.netlify.app/" target="_blank" rel="noreferrer">Saad Shaikh</a>.
        </p>
      </footer>

    </Fragment >
  )
}

export default IndexPage
