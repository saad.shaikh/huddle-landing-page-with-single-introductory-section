# Title: Huddle landing page with single introductory section

Tech stack: Gatsby.js

Deployed project: https://huddle-landing-page-with-single-introductory-section-khaki.vercel.app/

## Desktop view
![Desktop view](design/desktop-design.jpg) 

## Mobile view
![Mobile view](design/mobile-design.jpg) 